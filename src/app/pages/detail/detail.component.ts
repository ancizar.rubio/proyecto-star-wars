import { PeliculasService } from 'src/app/servicios/peliculas.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  idFilm:any
  detailAllFilm:any

  constructor(private peliculas:PeliculasService, private route:ActivatedRoute) { }

  ngOnInit() {
    this.idFilm = this.route.snapshot.paramMap.get("id")
    this.peliculas.getdetailpelicula(this.idFilm).subscribe(detail => {
      this.detailAllFilm = detail;

    })
  }

}
