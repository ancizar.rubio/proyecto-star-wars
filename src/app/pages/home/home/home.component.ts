import { Component, OnInit } from '@angular/core';
import { PeliculasService } from 'src/app/servicios/peliculas.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  peliculasall:any

  constructor(public peliculas:PeliculasService) { }

  ngOnInit() {
    this.peliculas.getpeliculas().subscribe(pelicula => {
      this.peliculasall=pelicula['results']
      console.log(this.peliculasall)
    })
  }

}
