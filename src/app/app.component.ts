import { Component, ElementRef, ViewChild, HostListener, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'star-wars';
  public sticky: boolean = false;

  @ViewChild('stickyMenu', {static:false}) menuElement: ElementRef;

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(e) {
    if (window.pageYOffset > 80) {
      this.sticky = true
    } else {
      this.sticky = false
    }
  }
}
