import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class PeliculasService {

  constructor(private http:HttpClient) { }

  getpeliculas() {
    return this.http.get(`http://swapi.py4e.com/api/films/`);
  }
  getdetailpelicula(id) {
    return this.http.get(`http://swapi.py4e.com/api/films/${id}/`);
  }
}
